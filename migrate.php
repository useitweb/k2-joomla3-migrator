<?php

class Migrate
{
    const MYSQL_BIN = '/Applications/XAMPP/xamppfiles/bin/mysql';
    const MYSQLADMIN_BIN = '/Applications/XAMPP/xamppfiles/bin/mysqladmin';
    const SQL_DUMP_PATH = '/Applications/XAMPP/htdocs/k2-joomla3-migrator/joomla3.sql';
    const JOOMLA_PATH = '/Applications/XAMPP/htdocs/joomla3';
    const AUTHOR_USER_ID = 42;
    const JOOMLA3_DB_NAME = 'joomla3';
    const JOOMLA3_DB_HOST = 'localhost';
    const JOOMLA3_DB_USER = 'root';
    const JOOMLA3_DB_PREFIX = 'vi58p_';
    const K2_DB_PREFIX = 'pbweb_';

    private $k2_db;
    private $categories = array();

    public function __construct()
    {
        $this->k2_db = new PDO('mysql:dbname=wildworks;host=localhost', 'root');

        define('JPATH_PLATFORM', self::JOOMLA_PATH.'/libraries');
        define('JPATH_LIBRARIES', self::JOOMLA_PATH.'/libraries');
        define('JPATH_BASE', self::JOOMLA_PATH);

        require_once JPATH_PLATFORM.'/import.legacy.php';

        $registry = JFactory::getConfig();

        $registry->set('dbtype', 'mysql');
        $registry->set('host', self::JOOMLA3_DB_HOST);
        $registry->set('user', self::JOOMLA3_DB_USER);
        $registry->set('db', self::JOOMLA3_DB_NAME);
        $registry->set('dbprefix', self::JOOMLA3_DB_PREFIX);
    }

    public function execute()
    {
        $this->reset_database();
        $this->migrate_categories();
        $this->migrate_items();
    }

    private function reset_database()
    {
        echo 'Confirm resetting database? y/n ';
        shell_exec(self::MYSQLADMIN_BIN.' -u root drop '.self::JOOMLA3_DB_NAME);
        shell_exec(self::MYSQLADMIN_BIN.' -u root create '.self::JOOMLA3_DB_NAME);
        shell_exec(self::MYSQL_BIN.' -u root '.self::JOOMLA3_DB_NAME.' < '.self::SQL_DUMP_PATH);
    }

    private function migrate_categories()
    {
        $categories = $this->k2_db->query('SELECT * FROM '.self::K2_DB_PREFIX.'k2_categories WHERE trash != 1')->fetchAll();
        foreach ($categories as $category)
        {
            $table = JTableCategory::getInstance('category', 'JTable');
            $table->bind(array(
                'extension' => 'com_content',
                'title' => $category['name'],
                'path' => $category['alias'],
                'alias' => $category['alias'],
                'description' => $category['description'],
                'published' => $category['published'],
                'created_user_id' => self::AUTHOR_USER_ID
            ));
            $table->store();
            $primary_keys = $table->getPrimaryKey();
            $this->categories[$category['id']] = $primary_keys['id'];
        }
    }

    private function migrate_items()
    {
        $items = $this->k2_db->query('SELECT * FROM '.self::K2_DB_PREFIX.'k2_items WHERE trash != 1')->fetchAll();
        foreach ($items as $item)
        {
            $table = JTableContent::getInstance('content', 'JTable');
            $table->bind(array(
                'title' => $item['title'],
                'alias' => $item['alias'],
                'catid' => $this->categories[$item['catid']],
                'state' => $item['published'],
                'publish_up' => $item['publish_up'],
                'publish_down' => $item['publish_down'],
                'introtext' => $item['introtext'],
                'fulltext' => $this->generate_fulltext($item),
                'hits' => $item['hits']
            ));
            $table->store();
        }
    }

    private function generate_fulltext($item)
    {
        $fulltext = $item['fulltext'];

        $fulltext .= (empty($item['video'])) ? NULL : '<p>'.$item['video'].'</p>';
        $fulltext .= (empty($item['video_caption'])) ? NULL : '<p>'.$item['video_caption'].'</p>';
        $fulltext .= (empty($item['video_credits'])) ? NULL : '<p>'.$item['video_credits'].'</p>';
        $fulltext .= (empty($item['image'])) ? NULL : '<p>'.$item['image'].'</p>';
        $fulltext .= (empty($item['image_caption'])) ? NULL : '<p>'.$item['image_caption'].'</p>';
        $fulltext .= (empty($item['image_credits'])) ? NULL : '<p>'.$item['image_credits'].'</p>';

        return $fulltext;
    }
}

$migrate = new Migrate;
$migrate->execute();
