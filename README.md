# k2-joomla3-migrator

Migrates content from K2 to Joomla3 native

# Setup

Modify the constants in `migrate.php`

# Run

`php migrate.php`
